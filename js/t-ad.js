var colorBoxes = 0;
var opaqueBoxes = 0;
var topLayer = 100;


$(document).ready( function initUI() {
    //Export workspace content as image
    $("#exportImage").click(function(){
        exportImage();
        return false;
    });
    //Remove all elements from workspace
    $("#clearWorkspace").click(function(){
        clearWorkspace();
        return false;
    });
    //Go back to editor
    $(window).scroll(function () {  
        if ( $(this).scrollTop() > 200 ) {
            $("#edit").show();
        } else {
            $("#edit").hide();
        }
    });
    $("#edit")
        .hide()
        .click(function(){
            $("html, body").animate({ scrollTop: 0 }, 400);
            return false;
        });
    //Add elements
    $("#addImage").click(function(){
        addImage( $("#imageLocation") );
        return false;
    });
    $("#addOpaqueBox").click(function(){
        addOpaqueBox(0.5);
        return false;
    });
    $("#addColorBox").click(function(){
        addColorBox( $("#colorBoxColor").val(), $("#colorBoxWidth").val(), $("#colorBoxHeight").val(), $("#colorBoxOpacity").val() );
        return false;
    });
    $("#addHeadline").click(function(){
        addTextBox("headline", $( "#headlineText").val());
        return false;
    });
    $("#addSubheadline").click(function(){
        addTextBox("subheadline", $( "#subheadlineText").val());
        return false;
    });
    $("#addIntro").click(function(){
        addTextBox("intro", $( "#introText").val());
        return false;
    });
    $("#addDescription").click(function(){
        addTextBox("description", $( "#descriptionText").val());
        return false;
    });
    $("#addLogo").click(function(){
        addLogo($("#logoText").val(), $("#logoScale").val(), $("#logoColor").val());
        return false;
    });
    //Clear background image
    $("#clearBackground").click(function(){
        $("#background > img").fadeOut(200, function() {
                $("#background").empty();
            });
        $("#changeBackground").val("");
        return false;
    });
    //Clear Selection
    $("#workspace").click(function(){
        clearSelection();
        return false;
    });
    //Remove selected element
    $("#deleteSelected").click(function(){
        deleteSelected();
        return false;
    });
    //Disable delete buttons
    $(".delete-button").attr("disabled", true);
    clearSelection();
    
    //jQueryUI Accordion
    $(".accordion").accordion({
        header: "h3",
        collapsible: true,
        animate: 20
    });
    //jQueryUI Spinners
    $("#colorBoxOpacity").spinner({
        step: 0.05,
        min: 0,
        max: 1,
        numberFormat: "n"
    });
    $("#logoScale").spinner({
        step: 0.1,
        min: 0.2,
        max: 3,
        numberFormat: "n"
    });
    //Colorpickers
    $("#colorBoxColor").colorpicker();
    $("#logoColor").colorpicker();
});

//html2canvas
function exportImage() {
    clearSelection();
    html2canvas( [ document.getElementById("workspace") ], {
        onrendered: function(canvas) {
            var image = document.createElement("img");
            image.src = canvas.toDataURL();
            $("#export").append(image);
            $("html, body").animate({ scrollTop: $(document).height() }, 400);
        }
    });
}

//New
function clearWorkspace() {
    $(".image, .colorbox, .opaquebox, .textbox, .logo, #background > img")
        .fadeOut(200, function() {
            $(".image, .colorbox, .opaquebox, .textbox, .logo").remove();
            $("#background").empty();
        });
    $("#export").empty();
}

function makeSelectable(elements) {
    $(elements).each( function(i) {
        $(this).click(function() {
            select($(this));
        return false;
        });
    });
    $("#workspace").selectable({
        filter: ".textbox, .colorbox, .opaquebox, .logo, .image",
        unselected: clearSelection
    });
}

function select(element) {
    clearSelection();
    element.toggleClass("ui-selected", 0);
    $("#contextMenu").show();
    position(element);
    $(element).children(".ui-resizable-handle").show();
    $(element).children().children(".ui-resizable-handle").show();
}

function clearSelection() {
    $(".ui-selected").removeClass("ui-selected");
    $("#contextMenu").hide();
    $(".ui-resizable-handle").hide();
}

function deleteButton(className) {
    $( "#" + className + "DeleteButton" )
        .attr("disabled", false)
        .click(function(){
            $( "." + className ).fadeOut(200, function() {
                $( "." + className ).remove();
            });
            $( "#" + className + "DeleteButton" ).attr("disabled", true);
            return false;
        });
}

function deleteSelected() {
    $(".ui-selected").fadeOut(200, function() {
        $(".ui.selected").remove();
    });
    clearSelection();
}

function setBackground(input) {
    var background = document.createElement("img");
    var reader = new FileReader();
    reader.onload = function (e) {
        $(background).attr("src", e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
    $("#background").html(background);
}

function position(onElement) {
    $( "contextMenu" ).position({
        of: onElement,
        my: "left top",
        at: "left top",
        collision: "flipfit flipfit"
    });
}

function addOpaqueBox(opacity) {   
    opaqueBoxes++;
    var container = document.createElement("div");
    var canvas = document.createElement("canvas");
    $(container)
        .appendTo("#workspace")
        .addClass("opaquebox")
        .attr("id", "opaquebox" + opaqueBoxes)
        .css("zIndex", topLayer++)
        .resizable({
            grid: [ 10,10 ]
        })
        .draggable({ 
            cursor: "move",
            containment: "#workspace",
            scroll: false,
            snap: "#background > img",
            snapTolerance: 10,
            grid: [ 10,10 ],
            stack: "#workspace div",
            drag: position($(container))
        });
    $(canvas)
        .attr("width", 700)
        .attr("height", 300)
        .appendTo(container);
    
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        var fill = "rgba(226, 0, 116, " + opacity + ")";
        ctx.fillStyle = fill;
        for (i=1; i<3; i++) {
            ctx.fillRect(Math.random()*10*i+30*i, Math.random()*10*i+20*i, Math.random()*500*i+100*i, Math.random()*500*i+100*i);
        }
        ctx.fillStyle = "rgba(226, 0, 116, 1)";
        ctx.fillRect(100, 100, 600, 200);
    }
    
    makeSelectable(container);
    $(container).trigger('click');
    deleteButton("opaquebox");
}

function addImage(input) {
    var container = document.createElement("div");
    var image = document.createElement("img");
    
    var reader = new FileReader();
    reader.onload = function (e) {
        $(image)
            .attr("src", e.target.result)
            .appendTo(container);
    };
    reader.readAsDataURL(input.files[0]);
    
    $(container)
        .appendTo("#workspace")
        .addClass("image")
        .css("zIndex", topLayer++)
        .resizable({
            //grid: [ 10,10 ],
            aspectRatio: true
        })
        .draggable({ 
            cursor: "move",
            containment: "#workspace",
            scroll: false,
            snap: "#background > img",
            snapTolerance: 10,
            grid: [ 10,10 ],
            stack: "#workspace div"
        });
    makeSelectable(container);
    $(container).trigger('click');
    deleteButton("image");
}

function addColorBox(color, width, height, opacity) {
    var container = document.createElement("div");
    colorBoxes++;
    $(container)
        .appendTo("#workspace")
        .addClass("colorbox")
        .attr("id", "colorbox" + colorBoxes)
        .css({
            zIndex: topLayer++,
            backgroundColor: color,
            width: width + "px",
            height: height + "px",
            opacity: opacity
        })
        .resizable({
            grid: [ 10,10 ]
        }).draggable({ 
            cursor: "move",
            containment: "#workspace",
            scroll: false,
            snap: "#background > img",
            snapTolerance: 10,
            grid: [ 10,10 ],
            stack: "#workspace div"
        });
    makeSelectable(container);
    $(container).trigger('click');
    deleteButton("colorbox");
}

function addTextBox(style, text) {
    var container = document.createElement("div");
    var textBox = document.createElement("p");
    $(textBox)
        .html(text)
        .appendTo(container);
    $(container)
        .appendTo("#workspace")
        .addClass("textbox")
        .addClass(style)
        .css("zIndex", topLayer++)
        .draggable({
            cursor: "move",
            containment: "#workspace",
            scroll: false,
            grid: [ 10,10 ],
            stack: "#workspace div"
        });
    makeSelectable(container);
    $(container).trigger('click');
    deleteButton(style);
    $(".description > p").resizable();
}

function addLogo(text, scale, color) {
    var container = document.createElement("div");
    var canvas = document.createElement("canvas");
    $(container)
        .appendTo("#workspace")
        .addClass("logo")
        .css("zIndex", topLayer++)
        .draggable({ 
            cursor: "move",
            containment: "#workspace",
            scroll: false,
            grid: [ 10,10 ],
            stack: "#workspace div"
        })
    $(canvas)
        .appendTo(container)
        .attr("width", scale*1100)
        .attr("height", scale*127);
    makeSelectable(container);
    $(container).trigger('click');
    deleteButton("logo");
    
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        
        ctx.clearRect(0,0,scale*1000,scale*127);
        if (color) ctx.fillStyle = color;
        else ctx.fillStyle = "rgba(226, 0, 116, 1)";
        
        //Scaling
        if (scale) var scaleFactor = scale;
        else var scaleFactor = 1;
        ctx.scale(scaleFactor,scaleFactor);
        
        ctx.beginPath();
        
        //Logo
        ctx.moveTo(2,0);
        ctx.lineTo(102,0);
        ctx.lineTo(104,37);
        ctx.lineTo(97,40);
        
        ctx.quadraticCurveTo(90,5,62,7);
        
        ctx.lineTo(62,110);
        
        ctx.quadraticCurveTo(62,120,86,120);
        ctx.lineTo(86,127);
        
        ctx.lineTo(18,127);
        
        ctx.lineTo(18,120);
        ctx.quadraticCurveTo(42,120,42,110);
        
        ctx.lineTo(42,7);
        
        ctx.quadraticCurveTo(14,5,7,40);
        ctx.lineTo(0,37);
        ctx.lineTo(2,0);
        
        //Digits
        ctx.moveTo(0,55);
        ctx.rect(0,55,28,28)
        
        ctx.moveTo(76,55);
        ctx.rect(76,55,28,28)
        
        ctx.moveTo(152,55);
        ctx.rect(152,55,28,28)
        
        ctx.moveTo(228,55);
        ctx.rect(228,55,28,28)
        
        //Text
        //TODO: Doesn"t use correct font unless loaded before.
        ctx.font = "36pt TeleGrotesk Headline Ultra, Arial";
        ctx.textBaseline = "middle";
        ctx.fillText(text, 396, 70);
        
        ctx.fill();
    }
}
